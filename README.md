# checkpoint-on-signal-example

Examples of how to catch a signal to checkpoint on demand.

Useful for taking advantage of discounts offered to jobs with flexible time requirements or preempt-able queues.

Examples in C++, C, Fortran and python.

C/C++ examples demonstrate use with and without MPI

## batch jobs

The SIGINT interupt signal will come from Slurm to every task in the job.

### note on srun

If `srun` is used to launch the program then `#SBATCH --signal=<SIGNAL>@<time>`
will send to all processes launched that way.

However, when `srun` is *not* used such as single node python then

`exec <executable>` is needed in combination with `#SBATCH --signal=B:<SIGNAL>@<time>`
because bash does not forward SIGINT.

## interactive

`cntl-C` to interrupt when running interactively.

`cntl-C` *twice* to interrupt when running interactively with `srun`.

