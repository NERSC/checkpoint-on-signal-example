
```
cookbg@cori08:~/src/checkpoint-on-signal-example/cxx> ls
Makefile  main.cpp  run.sh
cookbg@cori08:~/src/checkpoint-on-signal-example/cxx> make
CC -o checkpoint_on_signal_example.x main.cpp
CC -DUSEMPI -o checkpoint_on_signal_example.MPI.x main.cpp
cookbg@cori08:~/src/checkpoint-on-signal-example/cxx> ls
Makefile  checkpoint_on_signal_example.MPI.x  checkpoint_on_signal_example.x  main.cpp  run.sh
cookbg@cori08:~/src/checkpoint-on-signal-example/cxx> ./checkpoint_on_signal_example.x
0
1
2
^Crank(0): GOT SIGNAL 2
CHECKPOINTING...Got SIGINT... checkpointing
DONE
Got SIGINT... checkpointing
cookbg@cori08:~/src/checkpoint-on-signal-example/cxx> ls
Makefile  checkpoint_on_signal_example.MPI.x  checkpoint_on_signal_example.x  example.checkpoint  main.cpp  run.sh
cookbg@cori08:~/src/checkpoint-on-signal-example/cxx> cat example.checkpoint
2
cookbg@cori08:~/src/checkpoint-on-signal-example/cxx> cat run.sh
#!/bin/bash
#SBATCH --signal=SIGINT@60
#SBATCH --time=3
#SBATCH -C haswell
#SBATCH -N 2
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=2

# this needs 300 * 5s = 5 min to complete
srun ./checkpoint_on_signal_example.MPI.x

cookbg@gerty01:~/src/checkpoint-on-signal-example/cxx> sbatch run.sh
Submitted batch job 1909567
cookbg@gert01:~/src/checkpoint-on-signal-example/cxx> head slurm-1909567.out
restarting from : 2
2
3
4
5
6
7
8
9
10
cookbg@gert01:~/src/checkpoint-on-signal-example/cxx> tail slurm-1909567.out
rank(56): GOT SIGNAL 2
rank(57): GOT SIGNAL 2
rank(58): GOT SIGNAL 2
rank(59): GOT SIGNAL 2
rank(60): GOT SIGNAL 2
rank(61): GOT SIGNAL 2
rank(62): GOT SIGNAL 2
rank(63): GOT SIGNAL 2
Got SIGINT... checkpointing
CHECKPOINTING...DONE
```