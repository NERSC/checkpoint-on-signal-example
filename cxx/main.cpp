#include <csignal>
#include <iostream>
#include <thread>
#include <chrono>
#include <fstream>

#ifdef USEMPI
#include <mpi.h>
#endif

namespace
{
  volatile std::sig_atomic_t signal_status;
}

void signal_handler(int signal)
{
  signal_status = signal;
}

int myrank() {
#ifdef USEMPI
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  return rank;
#else
  return 0;
#endif
}

int checkpoint(int i) {
  std::ofstream fs("example.checkpoint");
  fs << i;
  fs.close();
  return 0;
}

int try_restart(int i)
{
  int rank = myrank();
  if (!rank)
    {
      std::ifstream fs("example.checkpoint");
      if (fs.good()) {
	fs >> i;
	fs.close();
	std::cout << "restarting from : " << i << std::endl;
      }
    }

#ifdef USEMPI
  MPI_Bcast(&i, 1, MPI_INT, 0, MPI_COMM_WORLD);
#endif  
  return i;
}

int main(int argc, char *argv[])
{
#ifdef USEMPI
  MPI_Init(&argc, &argv);
#endif

  int rank = myrank();

  std::signal(SIGINT, signal_handler);

  for (int i=try_restart(0); i<300; i++) {
    if (rank == 0) std::cout << "iteration = " << i << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(5));

    if (signal_status == SIGINT) {
      int checkpointok = 0;
      if (rank == 0) {
	std::cout << "Got SIGINT\nCheckpointing...";
	checkpointok = checkpoint(i);
	std::cout << "done" << std::endl;
      }
#ifdef USEMPI
      MPI_Bcast(&checkpointok, 1, MPI_INT, 0, MPI_COMM_WORLD);
      MPI_Finalize();
#endif
      return checkpointok;
    }
  }

#ifdef USEMPI
  MPI_Finalize();
#endif
  return 0;
}
