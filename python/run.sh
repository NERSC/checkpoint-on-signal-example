#!/bin/bash
#SBATCH --signal=B:SIGINT@60
#SBATCH --time=3
#SBATCH -C haswell
#SBATCH -N 1

# this needs 300 * 5s = 5 min to complete
# exec is needed since bash won't forward SIGINT!
exec ./main.py
