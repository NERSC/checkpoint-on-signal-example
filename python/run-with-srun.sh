#!/bin/bash
#SBATCH --signal=SIGINT@60
#SBATCH --time=3
#SBATCH -C haswell
#SBATCH -N 1
#SBATCH --ntasks-per-node=1

# this needs 300 * 5s = 5 min to complete
srun ./main.py
