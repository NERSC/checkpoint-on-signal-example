#!/bin/bash
#SBATCH --signal=B:SIGINT@60
#SBATCH --time=3
#SBATCH -C haswell
#SBATCH -N 1

cleanup ()
{
    echo $(date)": got the signal "
    exit
}

trap cleanup SIGINT

for i in {1..100}; do
    echo $(date) $i
    sleep 5
done
