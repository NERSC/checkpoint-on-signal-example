#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdatomic.h>

static volatile atomic_int signal_status = 0;

void signal_handler(int signal)
{
  signal_status = signal;
}

void c_register_handler()
{
  signal(SIGINT,signal_handler);
}

int c_test_for_sigint()
{
  return (signal_status == SIGINT);
}
