program example
  use m_sigint, only : test_for_sigint, register_handler
  use iso_fortran_env, only: output_unit
  implicit none
  integer :: i

  call register_handler()


  do i=try_restart(1), 300
     write(output_unit,*) "iteration = ", i
     call sleep(5)

     if (test_for_sigint()) then
        call checkpoint(i)
        stop 'checkpointed on interrupt'
     end if
  end do

contains

  function try_restart(i) result(j)
    integer, intent(in) :: i
    integer :: j
    logical :: file_exists
    inquire(file="example.checkpoint", exist=file_exists)
    if (file_exists) then
       open(unit=15, status="old", action="read", &
            file="example.checkpoint")
       read(15,*) j
       close(unit=15)
       write(output_unit, *) "Restarting from ", j
    else
       j=i
    endif
  end function try_restart

  subroutine checkpoint(i)
    integer, intent(in) :: i
    write(output_unit,*) "Got SIGINT"
    write(output_unit, "(A)", advance="no") "checkpointing..."
    open(unit=15, file="example.checkpoint")
    write(15, *) i
    close(unit=15)
    write(output_unit,*) "done"
  end subroutine checkpoint

end program example
