module m_sigint
  use, intrinsic :: iso_c_binding, only: c_int
  implicit none

  public :: test_for_sigint, register_handler
  private :: test_for_sigint_impl

  interface
     integer (c_int) function test_for_sigint_impl() &
          bind (C, name = "c_test_for_sigint")
       use, intrinsic :: iso_c_binding, only: c_int
     end function test_for_sigint_impl
  end interface

  interface
     subroutine register_handler() &
       bind (C, name = "c_register_handler")
     end subroutine register_handler
  end interface

contains

  function test_for_sigint() result(b)
    integer :: i
    logical :: b
    i = test_for_sigint_impl()
    if (i == 1) then
       b = .true.
    else
       b = .false.
    endif
  end function test_for_sigint

end module m_sigint
