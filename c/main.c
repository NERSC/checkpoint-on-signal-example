#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdatomic.h>

#ifdef USEMPI
#include <mpi.h>
#endif

static volatile atomic_int signal_status = 0;

void signal_handler(int signal)
{
  signal_status = signal;
}

int myrank()
{
#ifdef USEMPI
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  return rank;
#endif
  return 0;
}

int checkpoint(int i)
{
  FILE *fptr;
  if (fptr = fopen("example.checkpoint", "w"))
    {
      printf("checkpointing...");
      fprintf(fptr, "%i\n", i);
      printf("done\n");
      fclose(fptr);
      return 1;
    }
  fprintf(stderr, "Failed to checkpoint\n");
#ifdef USEMPI
  MPI_Abort(MPI_COMM_WORLD, 1);
#endif
  exit(1);
}

int try_restart(int i) {
  int rank = myrank();
  if ((!rank) && (!access("example.checkpoint", F_OK)))
    {
      FILE *fptr = fopen("example.checkpoint", "r");
      if (fptr != NULL)
	{
	  fscanf(fptr, "%d", &i);
	  fclose(fptr);
	  printf("Restarting with: %d\n", i);
	}
    }
#ifdef USEMPI
  MPI_Bcast(&i, 1, MPI_INT, 0, MPI_COMM_WORLD);
#endif
  return i;
}

int checkpoint_on_sigint(int i)
{
  int checkpointok = 0;
  if ( signal_status == SIGINT )
    {
      int rank = myrank();
      if (!rank)
	{
	  printf("Got SIGINT\n");
	  fflush(stdout);
	  checkpointok = checkpoint(i);
	}
#ifdef USEMPI
      MPI_Bcast(&checkpointok, 1, MPI_INT, 0, MPI_COMM_WORLD);
#endif
    }
  return checkpointok;
}

int main(int argc, char *argv[])
{
#ifdef USEMPI
  MPI_Init(&argc, &argv);
#endif
  int rank = myrank();

  signal(SIGINT,signal_handler);

  for (int i = try_restart(0); i < 300; i++)
    {
      if (!rank) printf("iteration = %d\n", i);
      sleep(5);

      if (checkpoint_on_sigint(i))
	{
#ifdef USEMPI
	  MPI_Finalize();
#endif
	  return 0;
	}
    }

#ifdef USEMPI
  MPI_Finalize();
#endif
  return 0;
}
