#!/bin/bash
#SBATCH --signal=SIGINT@60
#SBATCH --time=3
#SBATCH -C haswell
#SBATCH -N 2
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=2

# this needs 300 * 5s = 5 min to complete
srun ./checkpoint_on_signal_example.MPI.x

